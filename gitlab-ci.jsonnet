local param_job(script, parallel_count, _stage) =
  {
    stage: _stage,
    image: "alpine:latest",
    script: [script, "export"],
    parallel: parallel_count,
    rules: [
      { "if": "$CI_COMMIT_BRANCH" }
    ]
  };


{
  "stages": ["stage1", "stage2"],
  "rspec": param_job("echo Hello World", 2, "stage1"),
  "rspec 2": param_job("echo Hello World 2", 2, "stage2")
}
